# Catty

The programming language of cats.

===
[Join Our Discord Server](https://discord.gg/ypcMSagbtv)

===
Our YouTube Channel:
[**Click Here to Access**(https://www.youtube.com/channel/UCcp21eXmsQcbNIzbkZPpOeQ)](https://www.youtube.com/channel/UCcp21eXmsQcbNIzbkZPpOeQ)

## OK, what is Catty?

Catty is an object oriented, static, cross-platform, extensible and *localisable* programming language that runs on [Atatürk Runtime](https://github.com/CattyLang/ataturk-rt). It is designed by [Yiğit Cemal Öztürk](https://github.com/CadmiumC4), and empowered by the love for cats.
