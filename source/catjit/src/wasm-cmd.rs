pub mod CattyLang
{
    pub mod Runtime
    {
        pub enum WebAssemblyInstruction{
              Unreachable = 0x00,
              NOP,
              Block,
              Loop,
              If,
              Else,
              End = 0x0B,
              BR,
              BR_If,
              Return,
              Call,
              CallIndirect,
             r#Drop = 0x1A,
              Select,
              
              
        }
    }


}
